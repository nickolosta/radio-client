import { deepClone } from '../../lib/Utils'

export interface State {
  genres: any[]
};

export const initialState: State = {
  genres: []
};

export function Genres(state = initialState, action: any) {
  let newState = deepClone(state);
  if(action.hasOwnProperty('data')){
    const data = action.data;
    switch (action.type) {
      case "GENRES_GET_LIST":
        newState.genres = data.genres;
        return newState;
      default:
        return state;
    }
  } else {
    return state;
  }
}