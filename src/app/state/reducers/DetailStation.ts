import { deepClone } from '../../lib/Utils'

export interface State {
	station: any[]
};

export const initialState: State = {
	station: null
};

export function DetailStation(state = initialState, action: any) {
	let newState = deepClone(state);
  let data = null;
	if(action.hasOwnProperty('data')){
    data = action.data;
	}
  switch (action.type) {
    case "DETAIL_STATION":
    	if(data){
        newState.station = data.station;
			}
      return newState;
    case "DETAIL_STATION_CLEAR":
      newState.station = initialState.station;
      return newState;
    default:
      return state;

  }
}