import { deepClone } from '../../lib/Utils'

export interface State {
  userAuthData: any
};

export const initialState: State = {
  userAuthData: null
};

export function Login(state = initialState, action: any) {
  let newState = deepClone(state);
  const data = action.response;
  switch (action.type) {
    case "LOGIN_REQUEST":
      newState.userAuthData = data.user;
      return newState;
    // get authenticated user data
    case "LOGIN_GET_AUTH_USER_DATA":
      if(data){
        if(data.hasOwnProperty('user')){
          newState.userAuthData = data.user;
        }
      }
      return newState;
    case "LOGIN_LOGOUT":
      newState.userAuthData = null;
      return newState;
    default:
      return state;
  }
}
