import { deepClone } from '../../lib/Utils'

export interface State {
	stations: any[]
};

export const initialState: State = {
	stations: []
};

export function LastAddedStations(state = initialState, action: any) {
	let newState = deepClone(state);
	const data = action.data;
	switch (action.type) {
		case "LAST_ADDED_STATIONS_GET_LIST":
			newState.stations = data.stations;
			return newState;
		case "LAST_ADDED_STATIONS_LIST_CLEAR":
			newState.stations = initialState.stations;
			return newState;
		default:
			return state;
	}
}