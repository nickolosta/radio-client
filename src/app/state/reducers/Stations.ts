import {deepClone} from '../../lib/Utils'

export interface State {
	stations: any[],
	favorites: any[]
};

export const initialState: State = {
	stations: [],
	favorites: []
};

export function Stations(state = initialState, action: any) {
	let newState = deepClone(state);
	const data = action.data;
	switch (action.type) {
		case "STATIONS_GET_LIST":
			newState.stations = newState.stations.concat(data.stations);
			return newState;
		case "STATIONS_LIST_CLEAR":
			newState.stations = initialState.stations;
			return newState;
		case "FAVORITES_GET_LIST":
			newState.favorites = newState.favorites.concat(data.favorites);
			return newState;
		case "FAVORITES_LIST_CLEAR":
			newState.favorites = initialState.favorites;
			return newState;
		case "REMOVE_FROM_FAVORITE":
			newState.favorites = newState.favorites.filter((station) =>{
				return station.id != data.stationId;
			});
			return newState;
		default:
			return state;
	}
}