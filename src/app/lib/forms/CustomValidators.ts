import {FormControl} from '@angular/forms';
import * as moment from 'moment-timezone';

export default class CustomValidators {

  static email(control: FormControl): ValidationResult {
    const REGEXP = /^(([^<>()\[\]\\,;:\s@"]+(\.[^<>()\[\]\\,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!control.value){
      return null;
    }
    if (control.value != "" && (control.value.length <= 5 || !REGEXP.test(control.value))) {
      return { "email": true };
    }
    return null;
  }

  static minOneCharacter(control: FormControl): ValidationResult {
    const REGEXP = /[a-zA-Z]/;
    if(!control.value){
      return null;
    }
    if (control.value != "" && (control.value.length <= 5 || !REGEXP.test(control.value))) {
      return { "minOneCharacter": true };
    }
    return null;
  }

  static notSpecialChars(control: FormControl): ValidationResult {
    const REGEXP = /[^\!\@\#\$\%\^\&\*\(\)\_\+\.\,\;\:]/;
    if(!control.value){
      return null;
    }
    if (control.value != "" && (control.value.length <= 5 || !REGEXP.test(control.value))) {
      return { "notSpecialChars": true };
    }
    return null;
  }

  /** only format DD.MM.YYYY
   * FormControl should have value as moment object or string date format
   * */

  static dateLessThanCurrent(control: FormControl): ValidationResult {
    let date = control.value;
    if(!date){
      return null;
    }
    // if not moment date object
    if(typeof date !== 'object') {
      date = moment(date, "DD.MM.YYYY").startOf('day');
    } else {
      date = moment(date.formatted, "DD.MM.YYYY").startOf('day');
    }

    let currentDay = moment().startOf('day');
    if(currentDay.isAfter(date)) {
      return { "dateLessThanCurrent": true };
    }
    return null;
  }

}

interface ValidationResult {
  [key: string]: boolean;
}
