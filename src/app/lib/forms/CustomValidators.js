"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment-timezone");
var CustomValidators = (function () {
    function CustomValidators() {
    }
    CustomValidators.email = function (control) {
        var REGEXP = /^(([^<>()\[\]\\,;:\s@"]+(\.[^<>()\[\]\\,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!control.value) {
            return null;
        }
        if (control.value != "" && (control.value.length <= 5 || !REGEXP.test(control.value))) {
            return { "email": true };
        }
        return null;
    };
    CustomValidators.minOneCharacter = function (control) {
        var REGEXP = /[a-zA-Z]/;
        if (!control.value) {
            return null;
        }
        if (control.value != "" && (control.value.length <= 5 || !REGEXP.test(control.value))) {
            return { "minOneCharacter": true };
        }
        return null;
    };
    CustomValidators.notSpecialChars = function (control) {
        var REGEXP = /[^\!\@\#\$\%\^\&\*\(\)\_\+\.\,\;\:]/;
        if (!control.value) {
            return null;
        }
        if (control.value != "" && (control.value.length <= 5 || !REGEXP.test(control.value))) {
            return { "notSpecialChars": true };
        }
        return null;
    };
    /** only format DD.MM.YYYY
     * FormControl should have value as moment object or string date format
     * */
    CustomValidators.dateLessThanCurrent = function (control) {
        var date = control.value;
        if (!date) {
            return null;
        }
        // if not moment date object
        if (typeof date !== 'object') {
            date = moment(date, "DD.MM.YYYY").startOf('day');
        }
        else {
            date = moment(date.formatted, "DD.MM.YYYY").startOf('day');
        }
        var currentDay = moment().startOf('day');
        if (currentDay.isAfter(date)) {
            return { "dateLessThanCurrent": true };
        }
        return null;
    };
    return CustomValidators;
}());
exports.default = CustomValidators;
//# sourceMappingURL=CustomValidators.js.map