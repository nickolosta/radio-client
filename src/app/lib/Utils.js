"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment-timezone");
// deep clone for alternative spread/ Object.Assign
function deepClone(obj) {
    var copy;
    if (null == obj || "object" != typeof obj)
        return obj;
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = deepClone(obj[i]);
        }
        return copy;
    }
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr))
                copy[attr] = deepClone(obj[attr]);
        }
        return copy;
    }
}
exports.deepClone = deepClone;
function deepEqual(a, b) {
    if ((typeof a == 'object' && a != null) &&
        (typeof b == 'object' && b != null)) {
        var count = [0, 0];
        for (var key in a)
            count[0]++;
        for (var key in b)
            count[1]++;
        if (count[0] - count[1] != 0) {
            return false;
        }
        for (var key in a) {
            if (!(key in b) || !deepEqual(a[key], b[key])) {
                return false;
            }
        }
        for (var key in b) {
            if (!(key in a) || !deepEqual(b[key], a[key])) {
                return false;
            }
        }
        return true;
    }
    else {
        return a === b;
    }
}
exports.deepEqual = deepEqual;
function setCookie(cookieName, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cookieName + "=" + cvalue + ";" + expires + ";path=/";
}
exports.setCookie = setCookie;
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}
exports.getCookie = getCookie;
function delete_cookie(cookieName) {
    document.cookie = cookieName + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
exports.delete_cookie = delete_cookie;
// make URL string with get params
function createURL(URL, dataGETParams) {
    var ret = [];
    var paramsStr;
    for (var d in dataGETParams)
        ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(dataGETParams[d]));
    paramsStr = ret.join('&');
    return paramsStr.length > 0 ? URL + '?' + ret.join('&') : URL;
}
exports.createURL = createURL;
// translate array, because translateService has promise
function translateArray(translateService, ArrItems, attrName, callback, currentLanguage) {
    var arrItems = ArrItems;
    var arrItemsResult = ArrItems;
    var i = 0;
    var callbackTranslater = function (textTranslate, index) {
        i++;
        arrItemsResult[index][attrName] = textTranslate;
        if (i == ArrItems.length) {
            callback(arrItemsResult);
        }
    };
    arrItems.forEach(function (item, index) {
        translateService.get(item[attrName]).subscribe(function (text) { return callbackTranslater(text, index); });
    });
}
exports.translateArray = translateArray;
function trim(value) {
    return value ? value.trim() : value;
}
exports.trim = trim;
// rename list e.g [{title:"name 1", id: 1},...] to [{name:"name 1", value: 1},...]
function renameList(array, names) {
    return array.map(function (elm) {
        var newElm = {};
        for (var n in names) {
            if (elm[n]) {
                newElm[names[n]] = elm[n];
            }
        }
        return newElm;
    });
}
exports.renameList = renameList;
function uniqueId() {
    var d = new Date().getTime();
    return 'xxxxxxxxxxxxxxx-yxxx-xyxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}
exports.uniqueId = uniqueId;
function JSON_to_multipartData(json) {
    var formData = new FormData();
    for (var j in json) {
        var value = json[j];
        (value == null || value == 'null') ? value = '' : null;
        formData.append(j, value);
    }
    return formData;
}
exports.JSON_to_multipartData = JSON_to_multipartData;
//e.g. 30.10.1985 to 10-30-1985
function dateStringFormatToOtherDateString(dateString, formatFrom, formatTo) {
    return moment(dateString, formatFrom).format(formatTo);
}
exports.dateStringFormatToOtherDateString = dateStringFormatToOtherDateString;
// e.g trump to Trump
function stringToFamelyFormat(str) {
    if (str.length == 0) {
        return '';
    }
    var result = str.toLowerCase();
    result = str[0].toUpperCase() + result.slice(1, result.length);
    return result;
}
exports.stringToFamelyFormat = stringToFamelyFormat;
function browserName() {
    // Opera 8.0+
    var isOpera = (!!window.opr && !!window.opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    if (isOpera) {
        return 'Opera';
    }
    // Firefox 1.0+
    var isFirefox = typeof window.InstallTrigger !== 'undefined';
    if (isFirefox) {
        return 'Firefox';
    }
    // Safari 3.0+ "[object HTMLElementConstructor]"
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof window.safari !== 'undefined' && window.safari.pushNotification));
    if (isSafari) {
        return 'Safari';
    }
    // Internet Explorer 6-11
    var isIE = false || !!document.documentMode;
    if (isIE) {
        return 'IE';
    }
    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;
    if (isEdge) {
        return 'Edge';
    }
    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    if (isChrome) {
        return 'Chrome';
    }
    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;
    if (isBlink) {
        return 'Blink';
    }
    return 'undefined';
}
exports.browserName = browserName;
function truncate(string, length) {
    if (string.length > length)
        return string.substring(0, length) + '...';
    else
        return string;
}
exports.truncate = truncate;
;
function chunk_array(array, size) {
    var results = [];
    while (array.length) {
        results.push(array.splice(0, size));
    }
    return results;
}
exports.chunk_array = chunk_array;
//# sourceMappingURL=Utils.js.map