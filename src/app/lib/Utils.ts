import * as moment from 'moment-timezone';
import {browser} from "protractor";

// deep clone for alternative spread/ Object.Assign
function deepClone(obj) {
  let copy;
  if (null == obj || "object" != typeof obj) return obj;
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }
  if (obj instanceof Array) {
    copy = [];
    for (let i = 0, len = obj.length; i < len; i++) {
      copy[i] = deepClone(obj[i]);
    }
    return copy;
  }
  if (obj instanceof Object) {
    copy = {};
    for (let attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = deepClone(obj[attr]);
    }
    return copy;
  }
}

function deepEqual(a, b) {
  if ((typeof a == 'object' && a != null) &&
    (typeof b == 'object' && b != null)) {
    let count = [0, 0];
    for (let key in a) count[0]++;
    for (let key in b) count[1]++;
    if (count[0] - count[1] != 0) {
      return false;
    }
    for (let key in a) {
      if (!(key in b) || !deepEqual(a[key], b[key])) {
        return false;
      }
    }
    for (let key in b) {
      if (!(key in a) || !deepEqual(b[key], a[key])) {
        return false;
      }
    }
    return true;
  }
  else {
    return a === b;
  }
}

function setCookie(cookieName, cvalue, exdays) {
  let d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cookieName + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return null;
}

function delete_cookie(cookieName) {
  document.cookie = cookieName + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/';
}

// make URL string with get params
function createURL(URL: string, dataGETParams) {
  let ret = [];
  let paramsStr: string;
  for (let d in dataGETParams)
    ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(dataGETParams[d]));
  paramsStr = ret.join('&');
  return paramsStr.length >0 ? URL+'?'+ret.join('&') : URL;
}

// translate array, because translateService has promise
function translateArray(translateService, ArrItems, attrName, callback, currentLanguage) {
  let arrItems = ArrItems;
  let arrItemsResult = ArrItems;
  let i = 0;
  const callbackTranslater = (textTranslate, index) => {
    i++;
    arrItemsResult[index][attrName] = textTranslate;
    if(i == ArrItems.length){
      callback(arrItemsResult);
    }
  };

  arrItems.forEach(function (item, index) {
    translateService.get(item[attrName]).subscribe((text) => callbackTranslater(text, index))
  });
}

function trim(value){
    return value ? value.trim() : value;
}
// rename list e.g [{title:"name 1", id: 1},...] to [{name:"name 1", value: 1},...]
function renameList(array, names){
  return array.map(function(elm) {
    let newElm = {};
    for(let n in names){
      if(elm[n]){
        newElm[names[n]] = elm[n];
      }
    }
    return newElm;
  });
}

function uniqueId () {
  let d = new Date().getTime();
  return 'xxxxxxxxxxxxxxx-yxxx-xyxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    let r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
}

function JSON_to_multipartData(json) {
  const formData = new FormData();
  for(let j in json){
    let value = json[j];
    (value == null || value == 'null') ? value = '' : null;
    formData.append(j, value);
  }
  return formData;
}

//e.g. 30.10.1985 to 10-30-1985
function dateStringFormatToOtherDateString(dateString,formatFrom, formatTo) {
  return moment(dateString, formatFrom).format(formatTo);
}

// e.g trump to Trump
function stringToFamelyFormat(str) {
  if(str.length == 0){
    return '';
  }
  let result = str.toLowerCase();
  result = str[0].toUpperCase() + result.slice(1,result.length);
  return result;
}

function browserName(){
  // Opera 8.0+
  let isOpera = (!!(window as any).opr && !!(window as any).opr.addons) || !!(window as any).opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  if(isOpera) {
    return 'Opera'
  }
// Firefox 1.0+
  let isFirefox = typeof (window as any).InstallTrigger !== 'undefined';
  if(isFirefox) {
    return 'Firefox'
  }
// Safari 3.0+ "[object HTMLElementConstructor]"
  let isSafari = /constructor/i.test((window as any).HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof (window as any).safari !== 'undefined' && (window as any).safari.pushNotification));
  if(isSafari) {
    return 'Safari'
  }
// Internet Explorer 6-11
  let isIE = /*@cc_on!@*/false || !!(document as any).documentMode;
  if(isIE) {
    return 'IE'
  }
// Edge 20+
  let isEdge = !isIE && !!(window as any).StyleMedia;
  if(isEdge) {
    return 'Edge'
  }
// Chrome 1+
  let isChrome = !!(window as any).chrome && !!(window as any).chrome.webstore;
  if(isChrome) {
    return 'Chrome'
  }
// Blink engine detection
  let isBlink = (isChrome || isOpera) && !!(window as any).CSS;
  if(isBlink) {
    return 'Blink'
  }
  return 'undefined'
}

function truncate(string, length:number) {
  if (string.length > length)
    return string.substring(0,length)+'...';
  else
    return string;
};

function chunk_array(array, size){
  var results = [];
  while (array.length) {
    results.push(array.splice(0, size));
  }
  return results;
}

export {
  deepClone,
  deepEqual,
  setCookie,
  getCookie,
  delete_cookie,
  createURL,
  translateArray,
  trim,
  renameList,
  uniqueId,
  JSON_to_multipartData,
  dateStringFormatToOtherDateString,
  stringToFamelyFormat,
  browserName,
  truncate,
  chunk_array
}
