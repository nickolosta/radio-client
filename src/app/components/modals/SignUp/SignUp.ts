import { Component, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import {Modal} from '../';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {SignUpService} from '../../../services';

@Component({
  selector: 'singUp',
  template: require('./SignUp.html'),
  styles: [require('./SignUp.css')],
})
export class SignUp {
  @ViewChild('modal') modal: Modal;
  form: FormGroup;
  
  /** controls */
  name: FormControl;
  last_name: FormControl;
  email: FormControl;
  password: FormControl;
  password_confirmation: FormControl;
  /** controls [END]*/
  isDublicateEmail = false;
  isConfirmRegistration = false;
  AttempToSave = false;
  constructor(private formBuilder: FormBuilder, private viewContainerRef: ViewContainerRef, private modalService: NgbModal, private signUpService: SignUpService) {
    signUpService._showModal.subscribe((show) => {
      if(show){
        this.modal.show();
      }
    });
  }
  ngAfterViewInit() {

  }
  ngOnInit() {
    this.createForm();
  }
  ngOnDestroy() {
    this.toInit();
  }
  clearCustomErrors(){
    this.isDublicateEmail = false;
  }
  toInit(){
    this.AttempToSave = false;
    this.clearForm();
    this.clearCustomErrors();
    this.isConfirmRegistration = false;
  }
  callbackOk(){
    this.AttempToSave = true;
    this.form.updateValueAndValidity();
  }
  callbackCancel() {
    this.toInit();
  }
  createForm() {
    this.name = new FormControl('', Validators.compose([Validators.required]));
    this.last_name = new FormControl('', Validators.compose([Validators.required]));
    this.email = new FormControl('', Validators.compose([Validators.required]));
    this.password = new FormControl('', Validators.compose([Validators.required]));
    this.password_confirmation = new FormControl('', Validators.compose([Validators.required]));
    
    this.form = this.formBuilder.group({
      name: this.name,
      last_name: this.last_name,
      email: this.email,
      password: this.password,
      password_confirmation: this.password_confirmation
    })
  }
  getValidators(nameControl: string){
    switch(nameControl) {
      case "name":
      return [
        {errorText: "Name is required", validatorType: 'required'}
      ]
      case "last_name":
      return [
        {errorText: "Last name is required", validatorType: 'required'}
      ]
       case "email":
      return [
        {errorText: "Email is required", validatorType: 'required'}
      ]
       case "password":
      return [
        {errorText: "Password is required", validatorType: 'required'}
      ]
       case "password_confirmation":
      return [
        {errorText: "Retry password", validatorType: 'required'}
      ]
    }
  }
  clearForm() {
    this.AttempToSave = false;
    this.name.reset();
    this.last_name.reset();
    this.email.reset();
    this.password.reset();
    this.password_confirmation.reset();
  }
  saveSuccess(data) {
    this.isConfirmRegistration = true;
    console.log(data);
  }
  saveFail(err) {
    if(err){
      if(err.hasOwnProperty('email')){
        if(err.email.length > 0){
          if(err.email.indexOf('The email has already been taken.') > -1){
            this.isDublicateEmail = true;
          }
        }
      }
    }
    console.log(err, 'err');
  }
  save() {
    this.AttempToSave = true;
    if(this.form.valid){
      let data = this.form.value;
      this.signUpService.register(data,this.saveSuccess.bind(this), this.saveFail.bind(this))
    }
  }
}
