import { Component, ViewChild, ElementRef, Input, ViewEncapsulation } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'modal',
  template: require('./Modal.html'),
  styles: [require('./Modal.css')],
  encapsulation: ViewEncapsulation.None,
})
export class Modal  {
  @Input() title?:string;
  @Input() labelCancel?:string;
  @Input() labelOk?:string;
  @Input() callbackCancel:any = () => {};
  @Input() callbackOk:any = () => {};
  @ViewChild('content') content: ElementRef;
  modalRef = null;
  constructor(private modalService: NgbModal) {
  }
  show(){
    this.modalRef = this.modalService.open(this.content,{windowClass: "animated bounceInDown"} );
    this.modalRef.result.then((data) => {
      this.callbackCancel();
    }, (reason) => {
      this.callbackCancel();
    });
  }
  hide(){
    this.modalRef.close();
  }
  events(){

  }
  ngAfterViewInit() {
    this.events();
  }
}
