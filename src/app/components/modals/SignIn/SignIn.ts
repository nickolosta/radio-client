import { Component, ViewChild, ElementRef,ViewContainerRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Observable} from 'rxjs/Rx';
import {Modal} from '../';
import {SignInService} from '../../../services';

@Component({
  selector: 'sign-in',
  template: require('./SignIn.html'),
  styles: [require('./SignIn.css')]
})
export class SignIn {
  constructor(private formBuilder: FormBuilder, private viewContainerRef: ViewContainerRef, private signInService: SignInService) {
    signInService._showModal.subscribe((show) => {
      if(show){
        this.modal.show();
      }
    });
  }
  @ViewChild('modal') modal: Modal;
  form: FormGroup;
  /** controls */
  email: FormControl;
  password: FormControl;
  /** controls [END]*/
  AttempToSave = false;
  callbackCancel(){
    this.toInit();
  }
  ngAfterViewInit() {
  }
  ngOnInit() {
    this.createForm();
  }
  ngOnDestroy() {
    this.toInit();
  }
  toInit(){
    this.AttempToSave = false;
    this.clearForm();
    this.clearCustomErrors();
  }
  getValidators(nameControl: string){
    switch(nameControl) {
      case "email":
        return [
          {errorText: "Email is required", validatorType: 'required'}
        ];
      case "password":
        return [
          {errorText: "Password is required", validatorType: 'required'}
        ];
    }
  }
  clearForm() {
    this.AttempToSave = false;
    this.email.reset();
    this.password.reset();
  }
  clearCustomErrors(){
  }
  createForm() {
    this.email = new FormControl('', Validators.compose([Validators.required]));
    this.password = new FormControl('', Validators.compose([Validators.required]));
    this.form = this.formBuilder.group({
      email: this.email,
      password: this.password
    })
  }
  saveSuccess(data) {
    this.modal.hide();
    console.log(data);
  }
  saveFail(err) {
    console.log(err, 'err');
  }
  save() {
    this.AttempToSave = true;
    if(this.form.valid){
      let data = this.form.value;
      this.signInService.loginRequest(data,this.saveSuccess.bind(this), this.saveFail.bind(this))
    }
  }
}
