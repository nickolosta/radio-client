import { Component } from '@angular/core';
import {Store} from '@ngrx/store';
import {GenresService, StationsService, PlayerService} from '../../../services'

@Component({
  selector: 'station-list',
  template: require('./StationList.html'),
  styles: [require('./StationList.css')]
})
export class StationList {
  selectedGenreId = null;
  stations = [];
	stationPlayed = null;
	player = null;
	_isPlaying = false;
	subscribes: any = {};
  constructor(private store: Store<any>, private genresService: GenresService, private stationsService: StationsService, private playerService: PlayerService ){
		this.subscribes.selectedGenre = genresService.selectedGenre.subscribe((genre) => {
      if(genre || genre === 0){
        let genreId = genre;
        if(genre !== null && typeof genre === 'object'){
          genreId = genre.id;
        }
        if(this.selectedGenreId != genreId){
					this.selectedGenreId = genreId;
					this.stationsService.clearStations();
					this.stationsService.getListStations();
				}
      }
    });
		this.subscribes.stations = store.select(store => store.Stations).subscribe((data) => {
      this.stations = data.stations;
    });
		this.subscribes.player = playerService.player.subscribe((player) => {
    	this.player = player;
		});
  }
  ngOnInit(){
		this.subscribes.isPlayed = this.playerService.isPlayed.subscribe((_isPlaying) => {
			this._isPlaying = _isPlaying;
		});
		this.subscribes.stationPlayed = this.playerService.stationPlayed.subscribe((stationPlayed) => {
			if(stationPlayed){
				this.stationPlayed = stationPlayed;
			}
		});
	}
	ngOnDestroy() {
		this.unsubscribe();
	}
	unsubscribe(){
		for(let key in this.subscribes){
			this.subscribes[key].unsubscribe()
		}
	}
	ngAfterViewInit(){
  	setTimeout(() => {
		  this.playerService.showPlayer();
	  }, 1000);
	}
}
