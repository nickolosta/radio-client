import { Component } from '@angular/core';
import {Store} from '@ngrx/store';
import {StationsService, PlayerService} from '../../../services'

@Component({
  selector: 'favorites-list',
  template: require('./FavoritesList.html'),
  styles: [require('./FavoritesList.css')]
})
export class FavoritesList {
	favorites = [];
	stationPlayed = null;
	player = null;
	_isPlaying = false;
  constructor(private store: Store<any>, private stationsService: StationsService, private playerService: PlayerService ){
    store.select(store => store.Stations).subscribe((data) => {
      this.favorites = data.favorites;
    });
    playerService.player.subscribe((player) => {
    	this.player = player;
		});
  }
  ngOnInit(){
  	this.bindAll();
		this.playerService.isPlayed.subscribe((_isPlaying) => {
			this._isPlaying = _isPlaying;
		});
		this.playerService.stationPlayed.subscribe((stationPlayed) => {
			if(stationPlayed){
				this.stationPlayed = stationPlayed;
			}
		});
		this.getListStations();
	}
	ngOnDestroy(){
  	this.stationsService.clearFavoritesStations();
	}
	bindAll(){
  	this.removeFavorite = this.removeFavorite.bind(this);
	}
	removeFavorite(station){
		this.stationsService.removeFavorite(station.id);
	}
	getListStations(){
  	this.stationsService.getListFavoritesStations();
	}
}
