import { Component, Input } from '@angular/core';
import {trigger, state, style, animate, transition} from '@angular/animations';
import {GenresService, NavigatorService, PlayerService, StationsService} from '../../../services'
import {Store} from '@ngrx/store';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import {chunk_array} from '../../../lib/Utils'

@Component({
  selector: 'genre-list',
  template: require('./GenreList.html'),
  styles: [require('./GenreList.css')],
  animations: [
    trigger('visibleGenresList', [
      state('hidden', style({
        transform: 'translateY(100%)'
      })),
      state('showed',   style({
        transform: 'translateY(0%)'
      })),
      transition('hidden => showed', animate('100ms ease-in')),
      transition('showed => hidden', animate('100ms ease-out'))
    ])
  ]
})
export class GenreList  {
  genresChunks: any[] = [];
  genreId = null;
  visibleGenresList = 'hidden';
	visiblePlayer = 'hidden';
	isInitFirst = true;
	subscribes: any = {};
  constructor(private store: Store<any>, private playerService: PlayerService, private genresService: GenresService, private navigatorService: NavigatorService, private activatedRoute: ActivatedRoute, private router: Router, private stationsService: StationsService){
		this.subscribes.visiblePlayer = this.playerService.visiblePlayer.subscribe((visible) => {
			this.visiblePlayer = visible;
		});
		this.subscribes.visibleGenresList = this.genresService.visibleGenresList.subscribe((visible) => {
      this.visibleGenresList = visible;
    });
		this.subscribes.navigationEnd = router.events.subscribe((event) => {
      if(event instanceof NavigationEnd){
        this.getRouterParameter();
      }
    });
		this.subscribes.genresChunks = store.select(store => store.Genres).subscribe((data) => {
      if(data.genres.length >0){
        let chunkCount = Math.round(data.genres.length/3);
        if(chunkCount<1){
          chunkCount = 1;
        }
        if(chunkCount < 9){
          chunkCount = 9;
        }
        let genresChunks = chunk_array(data.genres,chunkCount);
        if(genresChunks.length > 3){
          genresChunks[2] = genresChunks[2].concat(genresChunks[3]);
          genresChunks.splice(3, 1);
        }
        this.genresChunks = genresChunks;
      } else {

      }
    });
  }
  ngOnInit(){
    //this.getRouterParameter();
    this.bindAll();
  }
	ngOnDestroy() {
		this.unsubscribe();
	}
	unsubscribe(){
		for(let key in this.subscribes){
			this.subscribes[key].unsubscribe()
		}
	}
  bindAll(){
    this.selectGenre = this.selectGenre.bind(this);
  }
  setRouteParams(genreId, withoutRedirect = false){
    this.genreId = genreId;
    if(!withoutRedirect){
      this.navigatorService.setRouterParameter('stations',{'genre-id': genreId});
    }
  }
  selectGenre(genre, withoutRedirect = false){
    this.setRouteParams(genre.hasOwnProperty('id') ? genre.id: genre, withoutRedirect);
		let genreId = typeof genre === 'object' ? genre.id: genre;
		//this.stationsService.clearStations();
		if(this.genresService.selectedGenre.getValue() != genreId){
			this.genresService.setSelectedGenre(genreId);
			this.genresService.getListGenres(genreId);
		}
  }
  getRouterParameter() {
    let genreId = this.activatedRoute.snapshot.queryParams['genre-id'];
    if(genreId){
      this.selectGenre(genreId, true);
    } else {
			if(this.isInitFirst){
				this.selectGenre(0, true);
				this.isInitFirst = false;
			}
    }
  }
  hideGenresList(){
    this.genresService.hideGenresList();
  }
}
