import { Component } from '@angular/core';
import {Store} from '@ngrx/store';
import {LastAddedStationsService, PlayerService} from '../../../services'

@Component({
  selector: 'last-added-stations',
  template: require('./LastAddedStations.html'),
  styles: [require('./LastAddedStations.css')]
})
export class LastAddedStations {
  selectedGenreId = null;
  stations = [];
	stationPlayed = null;
	player = null;
	_isPlaying = false;
  constructor(private store: Store<any>, private lastAddedStationsService: LastAddedStationsService, private playerService: PlayerService ){
    store.select(store => store.LastAddedStations).subscribe((data) => {
      this.stations = data.stations;
    });
    playerService.player.subscribe((player) => {
    	this.player = player;
		});
  }
  ngOnInit(){
		this.playerService.isPlayed.subscribe((_isPlaying) => {
			this._isPlaying = _isPlaying;
		});
		this.playerService.stationPlayed.subscribe((stationPlayed) => {
			if(stationPlayed){
				this.stationPlayed = stationPlayed;
			}
		});
		this.getListStations();
	}
	getListStations(){
  	this.lastAddedStationsService.getListStations();
	}
}
