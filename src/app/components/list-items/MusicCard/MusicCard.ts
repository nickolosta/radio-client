import { ChangeDetectionStrategy, Component, Input, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {GenresService, NavigatorService, PlayerService, DetailStationService} from '../../../services'
import {clearInterval} from "timers";

@Component({
  selector: 'music-card',
  template: require('./MusicCard.html'),
  styles: [require('./MusicCard.css')],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MusicCard  {
  @Input() station: any;
	@Input() callbackClose: any = null;
	@Input() isStationPlayed: boolean = false;
	@Input() soundPlayer: any;
	@Input()_isPlaying = false;
  @Input() showCloseButton = false;
  constructor(
  	private playerService: PlayerService,
		private navigatorService: NavigatorService,
		private detailStationService: DetailStationService,
		private activatedRoute: ActivatedRoute){
	}
	selectStation(){
  	//this.playerService.play(this.station);
		let url = this.activatedRoute.snapshot.url;
		let genreId = this.activatedRoute.snapshot.queryParams['genre-id'];
  	this.navigatorService.setRouterParameter(url.join('/'), {"genre-id": genreId,"station-id": this.station.id})
	}
	togglePlay(event){
		event.stopPropagation();
		if(this.soundPlayer){
			if(this.isPlaying()){
				this.playerService.stop();
			} else {
				this.playerService.play();
			}
		}
	}
	isPlaying(){
		console.log(this._isPlaying, '!_isPlaying');
		let isPlaying = false;
		if(this.soundPlayer){
			isPlaying = !this.soundPlayer.paused && !this.soundPlayer.ended;
		}
		return isPlaying;
	}
	ngOnChanges(){
		console.log('!!!!');
	}
  close(event){
  	event.stopPropagation();
  	if(this.callbackClose) {
			this.callbackClose(this.station);
	  } else {
		  this.navigatorService.removeRouterParameters(['station-id']);
	  }
	}
	getLogo(){
		if(this.station){
			if(this.station.logoFileInfo){
				return this.playerService.getLogoPath(this.station.id);
			} else {
				if(this.station.URL_logo){
					return this.station.URL_logo
				}
			}
		}
		return null;
	}
}
