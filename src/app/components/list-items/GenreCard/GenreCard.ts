import { Component, Input } from '@angular/core';

@Component({
  selector: 'genre-card',
  template: require('./GenreCard.html'),
  styles: [require('./GenreCard.css')]
})
export class GenreCard  {
  @Input() genre: any;
  @Input() isSelect: boolean = false;
  @Input() callbackSelect: (genre:any) => any = (genre) => {};
  select(){
    this.callbackSelect(this.genre);
  }
}
