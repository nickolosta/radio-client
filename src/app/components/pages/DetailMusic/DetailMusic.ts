import { Component } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import {PlayerService, DetailStationService, NavigatorService} from '../../../services'
import {Store} from '@ngrx/store';

@Component({
	selector: 'detail-music',
  template: require('./DetailMusic.html'),
  styles: [require('./DetailMusic.css')]
})

export class DetailMusic {
	station = null;
	subscribes: any = {};
	constructor(
		private playerService: PlayerService,
		private router:Router,
		private store: Store<any>,
		private detailStationService: DetailStationService,
		private navigatorService: NavigatorService,
		private activatedRoute: ActivatedRoute){
		this.subscribes.router = router.events.subscribe(event => {
			if(event instanceof NavigationEnd) {
				let stationId = this.activatedRoute.snapshot.queryParams['station-id'];
				if(this.station){
          if(stationId != this.station.id){
          	if(stationId){
              detailStationService.getDetailStation(stationId);
						} else {
              detailStationService.clearDetailStation();
						}
					}
				} else {
          if(stationId){
            detailStationService.getDetailStation(stationId);
          } else {
            detailStationService.clearDetailStation();
          }
				}
			}
		});
		this.subscribes.station = store.select(store => store.DetailStation.station).subscribe((station) => {
			this.station = station;
      if(station){
        if(this.playerService.stationPlayed.getValue() && station){
        	if(this.playerService.stationPlayed.getValue().id == station.id){
        		return;
	        }
        }
        this.playerService.play(station);
      }
			//console.log(station, 'station');
		});
	}
	ngOnDestroy() {
		this.unsubscribe();
	}
	unsubscribe(){
		for(let key in this.subscribes){
			this.subscribes[key].unsubscribe()
		}
	}
	getGenresName(){
		let genresName = "";
		let index = 0;
		this.station.genres.forEach((genre) => {
			index++;
			genresName += genre.name;
			if(index < this.station.genres.length){
				genresName += ', '
			}
		});
		return genresName;
	}
	hideDetail(){

	}
}
