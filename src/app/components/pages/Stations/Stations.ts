import { Component } from '@angular/core';
import {GenresService, StationsService} from '../../../services'

@Component({
  template: require('./Stations.html'),
  styles: [require('./Stations.css')]
})

export class Stations {
	loading = true;
  constructor(private genresService: GenresService, private stationsService: StationsService){
		this.stationsService.loading.subscribe((loading) => {
			this.loading = loading;
		});
	}
}
