import { Component } from '@angular/core';

@Component({
  template: require('./Favorites.html'),
  styles: [require('./Favorites.css')]
})

export class Favorites {}
