import { Component, Input } from '@angular/core';

@Component({
  selector: 'input-component',
  template: require('./InputComponent.html'),
  styles: [require('./InputComponent.css')]
})
export class InputComponent  {
  @Input() name: string;
  @Input() form: any;
  @Input() validators: any = [];
  @Input() formControl: any;
  @Input() formControlName: string;
  @Input() AttempToSave: boolean;
  @Input() type: string = 'text';
  changed = false;
  constructor(){}
  checkHiddenError(validator){
  if(this.formControl.errors){
      return typeof this.formControl.errors[validator.validatorType] === 'undefined';
    }else{
      return true;
    }
  }
}
