import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'preloader',
  template: require('./Preloader.html'),
  styles: [require('./Preloader.css')],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class Preloader {
  @Input() show: boolean = false;
}
