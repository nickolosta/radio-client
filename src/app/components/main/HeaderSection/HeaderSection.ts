import { Component, Input } from '@angular/core';

@Component({
  selector: '.header-section-component',
  template: require('./HeaderSection.html'),
  styles: [require('./HeaderSection.css')]
})
export default class HeaderSection  {
  @Input() name: string;
}
