import { Component,Output } from '@angular/core';
import {SignInService, SignUpService, NavigatorService} from '../../../services';
import {Store} from '@ngrx/store';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'header',
  template: require('./Header.html'),
  styles: [require('./Header.css')]
})
export default class Header {
  userAuthData: null;
	currentRoute = '';
  constructor(
  	private store: Store<any>,
	  private signInService: SignInService,
	  private signUpService:SignUpService,
	  private navigatorService:NavigatorService,
	  private router: Router){
    store.select(store => store.Login.userAuthData).subscribe((userAuthData) => {
      this.userAuthData = userAuthData;
    });
		router.events.subscribe((event) => {
			if(event instanceof NavigationEnd){
				this.currentRoute = event.url.split('?')[0];
			}
		});
  }
  logout(){
    this.signInService.logout();
	  let path = this.navigatorService.getRouterPath();
	  if(path == '/favorites'){
	  	this.navigatorService.setRouterParameter('/',{});
	  }
  }
  signIn(){
    this.signInService.setShowModal(true);
  }
  signUp(){
    this.signUpService.setShowModal(true);
  }
}
