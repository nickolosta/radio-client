import { Component, Input, ViewChild, ElementRef } from '@angular/core';
import {trigger, state, style, animate, transition} from '@angular/animations';
import {
	PlayerService,
	NavigatorService,
	GenresService,
	GlobalEventsService,
	StationsService} from '../../../services'
import {Store} from '@ngrx/store';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'player',
  template: require('./Player.html'),
  styles: [require('./Player.css')],
	animations: [
		trigger('visiblePlayer', [
			state('hidden', style({
				transform: 'translateY(100%)'
			})),
			state('showed',   style({
				transform: 'translateY(0%)'
			})),
			transition('hidden => showed', animate('100ms ease-in')),
			transition('showed => hidden', animate('100ms ease-out'))
		])
	]
})
export default class Player  {
	visiblePlayer = 'hidden';
	stationPlayed = null;
	soundPlayer = null;
	interval = null;
	_isPlaying = false;
	isConnected = false;
	selectedGenre = null;
	soundButtonClicked = false;
	coordinates = null;
	volume = 100;
	showSoundButtonRound = false;
	_setTimeoutHideSoundButonRd = null;
	subscribes: any = {};
	constructor(
		private store: Store<any>,
		private playerService: PlayerService,
		private navigatorService: NavigatorService,
		private activatedRoute: ActivatedRoute,
		private genresService: GenresService,
		private globalEventsService: GlobalEventsService,
		private stationsService: StationsService,
		private router: Router){
		this.subscribes.mouseUp = this.globalEventsService.mouseUp.subscribe((event) => {
			this.soundButtonClicked = false;
			this.showSoundButtonRound = false;
		});
		this.subscribes.mouseMove = this.globalEventsService.mouseMove.subscribe((coordinates) => {
			if(this.soundButtonClicked){
				if(this.coordinates){
					let volume = this.volume;
					let range = 0;
					if(this.coordinates.y > coordinates.y){
						range = (this.coordinates.y - coordinates.y)-20/100;
						if(range > 20){
							range = 20;
						}
						volume+= range;
					} else {
						if(this.coordinates.y != coordinates.y){
							range = (coordinates.y-this.coordinates.y)-20/100;
							if(range > 20){
								range = 20;
							}
							volume-= range;
						}
					}
					if(volume <= 100 && volume >=0){
						this.volume = volume;
					} else {
						if(volume > 100){
							this.volume = 100;
						} else {
							if(volume < 0){
								this.volume = 0;
							}
						}
					}
					this.setVolume();
				}
				this.coordinates = coordinates;
			}
		});
		this.subscribes.router = router.events.subscribe(event => {
			if(event instanceof NavigationEnd) {
				let genreId = this.activatedRoute.snapshot.queryParams['genre-id'];
				if(genreId){
					this.genresService.getGenre(genreId, (data) => {
						this.selectedGenre = data.genre;
					});
				}
			}
		});
		this.playerService.visiblePlayer.subscribe((visible) => {
			this.visiblePlayer = visible;
		});
		this.playerService.isPlayed.subscribe((isPlayed) => {
			this._isPlaying = isPlayed;
		});
		this.playerService.isConnected.subscribe((isConnected) => {
			this.isConnected = isConnected;
			if(!isConnected){
				this.playerService.isPlayed.next(false);
			}
		})
	}
	ngOnInit(){
		this.events();
		this.playerService.stationPlayed.subscribe((stationPlayed) => {
			if(stationPlayed){
				this.stationPlayed = stationPlayed;
				if(this.soundPlayer){
					this.soundPlayer.src = stationPlayed.urlSound;
					this.soundPlayer.play();
				} else {
					if(stationPlayed.urlSound){
						this.soundPlayer = new Audio(stationPlayed.urlSound);
						this.soundPlayer.load();
						this.playerService.setPlayer(this.soundPlayer);
						this.playerService.play();
					}
				}

			}
		});
	}
	ngOnDestroy(){
		clearInterval(this.interval);
	}
	events(){
		setInterval(() => {
			if(!this.isPlaying() || !this.isConnected){
				if(this.soundPlayer){
					this.soundPlayer.pause();
				}
				this.playerService.isPlayed.next(false);
			}
		}, 1000);
	}
	isPlaying(){
		let isPlaying = false;
		if(this.soundPlayer){
			isPlaying = !this.soundPlayer.paused && !this.soundPlayer.ended;
		}
		this._isPlaying = isPlaying;
		return isPlaying;
	}
	togglePlay(){
		if(this.soundPlayer){
			if(this.isPlaying()){
				this.playerService.stop();
			} else {
				this.playerService.play();
			}
		}
	}
	getLogo(){
		if(this.stationPlayed){
		  if(this.stationPlayed.logoFileInfo){
        return this.playerService.getLogoPath(this.stationPlayed.id);
      } else {
		    if(this.stationPlayed.URL_logo){
		    	if(this.stationPlayed.URL_logo == 'none'){
				    return null;
			    }
          return this.stationPlayed.URL_logo
        }
      }
    }
    return null;
	}
	getGenreName(){
		if(this.selectedGenre){
			return this.selectedGenre.name;
		}
		return 'all_genres';
	}
	showGenresList(){
		this.genresService.showGenresList();
	}
	mousedown(){
		this.soundButtonClicked = true;
	}
	setVolume(){
		let volume = this.volume/100;
		this.playerService.setVolume(volume);
		clearTimeout(this._setTimeoutHideSoundButonRd);
	}
	showSoundButtonRd(){
		this.showSoundButtonRound = true;
		if(this.showSoundButtonRound){this.setTimeoutHideSoundButonRd()}
	}
	setTimeoutHideSoundButonRd(){
		clearTimeout(this._setTimeoutHideSoundButonRd);
		this._setTimeoutHideSoundButonRd = setTimeout(() => {
			this.showSoundButtonRound = false;
		}, 2000);
	}
	mute(){
		if(this.volume > 0){
			this.volume = 0;
		} else {
			this.volume = 100;
		}
		this.setVolume();
	}
	addToFavorite(){
		if(this.stationPlayed){
			this.stationsService.addToFavorite(this.stationPlayed.id, () => {
				let path = this.navigatorService.getRouterPath();
				if(path == '/favorites'){
					this.stationsService.clearFavoritesStations();
					this.stationsService.getListFavoritesStations();
				}
			});
		}
	}
	showDetail(){
		this.navigatorService.updateRouterParameter({'station-id': this.stationPlayed.id});
	}
}
