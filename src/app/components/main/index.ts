import Header from './Header/Header'
import Footer from './Footer/Footer'
import HeaderSection from './HeaderSection/HeaderSection'
import Player from './Player/Player'

export {
  Header,
  Footer,
  HeaderSection,
  Player
}
