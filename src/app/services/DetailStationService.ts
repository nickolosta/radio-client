/**
 * Created by nick on 08.09.17.
 */
import {Injectable} from "@angular/core";
import config from '../config/env.config'
import {API, Auth} from '../services/index'
import {createURL} from '../lib/Utils'
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Store} from '@ngrx/store';

@Injectable()
export class DetailStationService {
  constructor (private api: API, private store: Store<any>) {}
  private ROOT_URL = config.API.URL;
  // paths requests
  PATHS = {
    detailStation: this.ROOT_URL+'detail-station',
    getLogoPath: this.ROOT_URL+'get-logo',
  };
  /** Requests **/
	getDetailStation(stationId){
		// create URL with params
		let URL = createURL(
			this.PATHS.detailStation,
			{'station-id': stationId}
		);
		return this.api.get(
			URL,
			{},
// reducer callback
			(res: any) => {
				return { type: "DETAIL_STATION", data: res.data}
			}
		);
	}
  /** Requests[END] **/
  clearDetailStation(){
		this.store.dispatch({type: "DETAIL_STATION_CLEAR"});
	}

}
