import {Injectable} from "@angular/core";
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {setCookie, getCookie, delete_cookie} from '../../lib/Utils'

@Injectable()
export class Auth{
  public dayCookieLife = 1;
  public "t": BehaviorSubject<string> = new BehaviorSubject(null);
  //private "t" = null;
  setApiKey(key: string){
    setCookie('t', key, this.dayCookieLife);
    return this["t"].next(key);
  }
  getApiKey(){
    let key = getCookie('t');
    if(key != null && this["t"].getValue() == null){
      this["t"].next(key);
    }
    return this["t"];
  }
  removeApiKey(){
    delete_cookie('t');
    this["t"].next(null);
  }
  logout(){
    this.removeApiKey();
  }
}
