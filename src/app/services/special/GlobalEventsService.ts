import {Injectable} from "@angular/core";
import { Http, Response, Headers } from '@angular/http';
import {Observable, Subject} from 'rxjs';
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class GlobalEventsService {
	public mouseUp: Subject<any> = new Subject();
	public mouseMove: Subject<any> = new Subject();
	canSetMouseMove = true;
  constructor () {
		this.setItervalCanSetMouseMove();
  }
  setMouseMove(event){
  	if(this.canSetMouseMove){
		  this.mouseMove.next({y: event.pageY, x: event.pageX});
		  this.canSetMouseMove = false;
	  }
  }
  setItervalCanSetMouseMove(){
	  setInterval(() => {
		  this.canSetMouseMove = true;
	  }, 30);
  }
}
