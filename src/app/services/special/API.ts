import {Injectable} from "@angular/core";
import { Http, Response, Headers } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Store, Action} from '@ngrx/store';
import config from '../../config/env.config'
import {Auth} from './Auth'

@Injectable()
export class API {
  constructor (private auth: Auth, private http: Http, private store: Store<any>) {}

  /** for merge default and custom headers put params to options like:
   *  { headers: {
   *      headerName1: "headerValue1",
   *      headerName2: "headerValue2"
   *      ...
   *    },
   *
   *   //other options
   *    ...
   *  }
   * */
  getHeaders(customHeaders){
    let headers = new Headers();
    if(typeof customHeaders['Content-Type'] == 'undefined'){
      headers.append('Content-Type', 'application/json');
    }else{
      if(customHeaders['Content-Type'] == 'multipart/form-data'){
        delete customHeaders['Content-Type'];
      }
    }
    headers.append('t', this.auth.getApiKey().getValue());
    for (let c in customHeaders) {
      headers.append(c, customHeaders[c]);
    }
    return headers;
  }

  /** REST API GET request method */
  get(URL: string, options: any, actionCallback: (res: any) => Action){
    options.headers = this.getHeaders(options.headers || {});
    return this.http.get(URL, options).
    map((res:Response) => (actionCallback(res.json()))).
    catch((error:any) => Observable.throw(error.json().error || 'Server error')).subscribe(
      action => {this.store.dispatch(action)},
      err => {
        console.log(err);
      });
  }
  /** REST API POST request method */
  post(URL: string, body: Object, options: any, actionCallback: (res: any) => Action, errorCallback:(error:any) => any){
    options.headers = this.getHeaders(options.headers || {});
    return this.http.post(URL, body, options).
    map((res:Response) => (actionCallback(res.json())))
    .subscribe(
      action => {this.store.dispatch(action)},
      err => {
        errorCallback(err);
      });
  }

}
