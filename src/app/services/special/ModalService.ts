import {Injectable} from "@angular/core";
import {Subject} from 'rxjs/Subject';
import { Router } from "@angular/router";
import {deepClone} from "../../lib/Utils";

@Injectable()
export class ModalService {
  // changed something form status
  changedForm: boolean = false;
  newAlert: Subject<any> = new Subject();
  showAddToGoupModalEvent: Subject<any> = new Subject();
  showCreateGroupModalEvent: Subject<any> = new Subject();

  constructor(private router: Router){}

  /** show modals */
  showFormChangedAlert(callbackConfirm) {
    if(this.changedForm){
      this.newAlert.next({callbackConfirm});
    } else {
      callbackConfirm();
    }
  }

  showAddToGoupModal(_data) {
    let data = deepClone(_data);
    data.students = data.students || [];
    data.teachers = data.teachers || [];
    if(data.students.length >0 || data.teachers.length>0){
      data.typeModal = data.students.length > 0 ? 'students' : 'teachers';
      this.showAddToGoupModalEvent.next({data});
    }
  }
  showCreateGroupModal(_data) {
    let data = deepClone(_data);
    data.students = data.students || [];
    data.teachers = data.teachers || [];
    if(data.students.length >0 || data.teachers.length>0){
      data.typeModal = data.students.length > 0 ? 'students' : 'teachers';
      this.showCreateGroupModalEvent.next({data});
    }
  }
  /** show modals[END] */

  setChangedForm(value: boolean) {
    this.changedForm = value;
  }
  //activate route
  canActivate(route) {
    if (!this.changedForm) {
      return true;
    } else {
      const url = route.url.join('/');
      this.showFormChangedAlert(() => {
        this.setChangedForm(false);
        this.router.navigate([url]);
      });
      return false;
    }
  }

}
