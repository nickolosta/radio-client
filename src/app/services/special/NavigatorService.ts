import {Injectable} from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {deepClone} from '../../lib/Utils'
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/operator/filter';

@Injectable()
export class NavigatorService {
  previusRouterParams = {};
  previousPath = null;
  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .pairwise().subscribe((event) => {
      if(Array.isArray(event)){
        let Url  = (event as any)[0].url;
        let a = document.createElement('a');
        a.href = Url;
        this.previousPath = a.pathname;
        console.log(this.previousPath, 'this.previousPath');
      }
    });
  }
  // set new router with new query params
  setRouterParameter(path: string, params) {
    let oldParams:any = deepClone(this.getRouterParameters());
    this.previusRouterParams = deepClone(oldParams);
    //this.previousPath = this.getRouterPath();
    this.router.navigate([path],{ queryParams: params});
  }
  // get all query params
  getRouterParameters(){
    return this.activatedRoute.snapshot.queryParams;
  }
  // get param by name
  getRouterParameter(name){
    let params = this.getRouterParameters();
    if(params){
      for(let key in params){
        if(key == name) {
          return params[key];
        }
      }
    }
    return null;
  }
  // update query params
  updateRouterParameter(params: any, clearOtherParams = false, exclude = []){
    let oldParams:any = deepClone(this.getRouterParameters());
    this.previusRouterParams = deepClone(oldParams);
    if(!clearOtherParams){
      for(let key in params){
        oldParams[key] = params[key];
      }
      exclude.forEach((element) => {
        delete oldParams[element];
      });
      this.setRouterParameter(this.getRouterPath(), oldParams);
    } else {
      exclude.forEach((element) => {
        delete params[element];
      });
      this.setRouterParameter(this.getRouterPath(), params);
    }
  }
  // get current router path
  getRouterPath(){
    return location.pathname;
  }
  //remove query params
  removeRouterParameters(exclude = []){
    let oldParams:any = deepClone(this.getRouterParameters());
    this.previusRouterParams = deepClone(oldParams);
    exclude.forEach((element) => {
      delete oldParams[element];
    });
    this.setRouterParameter(this.getRouterPath(), oldParams);
  }
  getPreviusParameter(param){
    if(this.previusRouterParams.hasOwnProperty(param)){
      return this.previusRouterParams[param];
    }
    return null;
  }
  getPreviousPath(){
    return this.previousPath;
  }
}
