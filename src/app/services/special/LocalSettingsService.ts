import {Injectable} from "@angular/core";

@Injectable()
export class LocalSettingsService{
  getLanguage(): string {
    return localStorage.getItem('language');
  }

  setLanguage(language: string) {
    localStorage.setItem('language', language);
  }
  // hide/show left menu
  toggleMenu() {
    let menuShow = localStorage.getItem('menuShow') === "true";
    if (localStorage.getItem('menuShow') !== null) {
      menuShow = !menuShow;
    }else{
      menuShow = false;
    }
    localStorage.setItem('menuShow', menuShow.toString());
    return menuShow;
  }
  setMenu(menuShow: boolean){
    localStorage.setItem('menuShow', menuShow.toString());
    return menuShow;
  }
  getMenu() {
    let menuShow = localStorage.getItem('menuShow');
    return menuShow == null ? true : localStorage.getItem('menuShow') === "true";
  }
}
