import {Injectable} from "@angular/core";
import config from '../config/env.config'
import {API, Auth} from '../services/index'
import {createURL, delete_cookie} from '../lib/Utils'
import {Store} from '@ngrx/store';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class SignInService {
  constructor (private api: API, private auth: Auth, private store: Store<any>) {}
  private ROOT_URL = config.API.URL;
  // paths requests
  PATHS = {
    login: this.ROOT_URL+'get-token',
    getUserData: this.ROOT_URL+'user/data'
  };
  public _showModal: BehaviorSubject<boolean> = new BehaviorSubject(false);
  // send data to server
  loginRequest(data, successCallback: any, errorCallback: any) {
    // create URL with params
    let URL = createURL(
      this.PATHS.login,
      {}
    );
    return this.api.post(
      URL,
      data,
      {},
      // reducer callback
      (res: any) => {
        if(!res.success){
          errorCallback(res.errors || null);
	        return { type: ""}
        } else {
          this.auth.setApiKey(res.data.token);
          successCallback(res.data);
        }
        return { type: "LOGIN_REQUEST", response: res.data}
      },
      (error: any) => (errorCallback(error))
    );
  }

  // get auth user data
  getAuthUserDataRequest() {
    return this.api.get(
      this.PATHS.getUserData,
      {},
// reducer callback
      (res: any) => {
        return { type: "LOGIN_GET_AUTH_USER_DATA", response: res.data}
      }
    );
  }
  logout(){
    this.store.dispatch({type:'LOGIN_LOGOUT'});
	  delete_cookie('t');
  }
  setShowModal(show = false){
    this._showModal.next(show);
  }
}
