/**
 * Created by nick on 08.09.17.
 */
import {Injectable} from "@angular/core";
import config from '../config/env.config'
import {API, Auth} from '../services/index'
import {createURL} from '../lib/Utils'
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {GenresService} from'../services'
import {Store} from '@ngrx/store';

@Injectable()
export class StationsService {
	offset = 0;
	offsetFavorites = 0;
	public loading: BehaviorSubject<boolean> = new BehaviorSubject(false);
  constructor (private api: API, private genresService: GenresService, private store: Store<any>) {}
  private ROOT_URL = config.API.URL;
  // paths requests
  PATHS = {
    listStations: this.ROOT_URL+'list/stations',
	  listFavorites: this.ROOT_URL+'list/favorites',
	  addToFavorite: this.ROOT_URL+'create-favorite',
	  removeFavorite: this.ROOT_URL+'remove-favorite'
  };
  /** Requests **/
  getListStations() {
  	this.showPreloader();
    // create URL with params
    let URL = createURL(
      this.PATHS.listStations,
      {
      	'genre_id': this.genresService.selectedGenre.getValue(),
				limit: 16,
				offset: this.offset++
      }
    );
    return this.api.get(
      URL,
      {},
// reducer callback
      (res: any) => {
				this.hidePreloader();
				if(res.data.stations.length == 0){
					this.offset--;
				}
        return { type: "STATIONS_GET_LIST", data: res.data}
      }
    );
  }
	getListFavoritesStations() {
		this.showPreloader();
		// create URL with params
		let URL = createURL(
			this.PATHS.listFavorites,
			{
				limit: 16,
				offset: this.offsetFavorites++
			}
		);
		return this.api.get(
			URL,
			{},
// reducer callback
			(res: any) => {
				this.hidePreloader();
				if(res.data.favorites.length == 0){
					this.offset--;
				}
				return { type: "FAVORITES_GET_LIST", data: res.data}
			}
		);
	}
	addToFavorite(stationId, successcallback = () => {}){
// create URL with params
		let URL = createURL(
			this.PATHS.addToFavorite,
			{}
		);
		return this.api.post(
			URL,
			{station_id: stationId},
			{},
			// reducer callback
			(res: any) => {
				successcallback();
				return { type: "ADD_TO_FAVORITE", response: res.data}
			},
			(error: any) => ({})
		);
	}
	removeFavorite(stationId){
// create URL with params
		let URL = createURL(
			this.PATHS.removeFavorite,
			{}
		);
		return this.api.post(
			URL,
			{station_id: stationId},
			{},
			// reducer callback
			(res: any) => {
				console.log(res, 'res!');
				if(res.success){
					return { type: "REMOVE_FROM_FAVORITE", data:{stationId}}
				} else {
					return { type: ""}
				}
			},
			(error: any) => ({})
		);
	}
  /** Requests[END] **/
	clearStations(){
		this.offset = 0;
		this.store.dispatch({type:"STATIONS_LIST_CLEAR"})
	}
	clearFavoritesStations(){
		this.store.dispatch({type:"FAVORITES_LIST_CLEAR"})
	}
	showPreloader(){
		this.loading.next(true);
	}
	hidePreloader(){
		this.loading.next(false);
	}

}
