/**
 * Created by nick on 08.09.17.
 */
import {Injectable} from "@angular/core";
import config from '../config/env.config'
import {API, Auth} from '../services/index'
import {createURL} from '../lib/Utils'
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {GenresService} from'../services'
import {Store} from '@ngrx/store';

@Injectable()
export class LastAddedStationsService {
  constructor (private api: API, private genresService: GenresService, private store: Store<any>) {}
  private ROOT_URL = config.API.URL;
  // paths requests
  PATHS = {
    listStations: this.ROOT_URL+'lastadded/stations'
  };
  /** Requests **/
  getListStations() {
    // create URL with params
    let URL = createURL(
      this.PATHS.listStations,
			{}
    );
    return this.api.get(
      URL,
      {},
// reducer callback
      (res: any) => {
        return { type: "LAST_ADDED_STATIONS_GET_LIST", data: res.data}
      }
    );
  }
  /** Requests[END] **/
	clearStations(){
		this.store.dispatch({type:"STATIONS_LIST_CLEAR"})
	}
}
