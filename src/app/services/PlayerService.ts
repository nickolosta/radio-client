/**
 * Created by nick on 08.09.17.
 */
import {Injectable} from "@angular/core";
import config from '../config/env.config'
import {API, Auth} from '../services/index'
import {createURL} from '../lib/Utils'
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';

@Injectable()
export class PlayerService {
	public isPlayed: BehaviorSubject<boolean> = new BehaviorSubject(false);
	public stationPlayed: BehaviorSubject<any> = new BehaviorSubject(null);
	public visiblePlayer: BehaviorSubject<string> = new BehaviorSubject('hidden');
	public player: BehaviorSubject<any> = new BehaviorSubject(null);
	public isConnected: Observable<boolean>;

	constructor (private api: API) {
		this.isConnected = Observable.merge(
			Observable.of(navigator.onLine),
			Observable.fromEvent(window, 'online').map(() => {return true}),
			Observable.fromEvent(window, 'offline').map(() => {return false}));
	}
	private ROOT_URL = config.API.URL;
	// paths requests
	PATHS = {
    getLogoPath: this.ROOT_URL+'get-logo'
	};
	/** Requests **/

	/** Requests[END] **/
	setPlayer(player){
		let prevPlayer: any = this.player.getValue();
		if(prevPlayer){
			if(!prevPlayer.paused){
				prevPlayer.pause();
			}
		}
		this.player.next(player);
	}
	play(station = null){
		this.showPlayer();
		if(station){
			this.stationPlayed.next(station);
		}
		let player = this.player.getValue();
		player.play();
		this.isPlayed.next(true);
	}
	stop(){
		let player = this.player.getValue();
		player.pause();
		this.isPlayed.next(false);
	}
	showPlayer(){
		this.visiblePlayer.next('showed');
	}
	hidePlayer(){
		this.visiblePlayer.next('hidden');
	}
  getLogoPath(stationId){
    return createURL(
      this.PATHS.getLogoPath,
      {'station-id': stationId}
    );
  }
  setVolume(volume){
	  let player = this.player.getValue();
	  if(player){
		  player.volume = volume;
	  }
  }
}
