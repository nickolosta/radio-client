/**
 * Created by nick on 08.09.17.
 */
import {Injectable} from "@angular/core";
import config from '../config/env.config'
import {API, Auth} from '../services/index'
import {createURL} from '../lib/Utils'
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class GenresService {
  public selectedGenre: BehaviorSubject<any> = new BehaviorSubject(null);
  public visibleGenresList: BehaviorSubject<string> = new BehaviorSubject('hidden');
  constructor (private api: API) {}
  private ROOT_URL = config.API.URL;
  // paths requests
  PATHS = {
    listGenres: this.ROOT_URL+'list/genres',
	  getGenre: this.ROOT_URL+'genre'
  };
  /** Requests **/
  getListGenres(genreId) {
    // create URL with params
    let URL = createURL(
      this.PATHS.listGenres,
      {'genre_id': genreId}
    );
    return this.api.get(
      URL,
      {},
// reducer callback
      (res: any) => {
        return { type: "GENRES_GET_LIST", data: res.data}
      }
    );
  }
	getGenre(genreId, successCallback) {
		// create URL with params
		let URL = createURL(
			this.PATHS.getGenre,
			{'genre_id': genreId}
		);
		return this.api.get(
			URL,
			{},
// reducer callback
			(res: any) => {
				if(res.success){
					successCallback(res.data);
				}
				return { type: "GENRE_INFO", data: res.data}
			}
		);
	}
  /** Requests[END] **/
  setSelectedGenre(genreId){
    this.selectedGenre.next(genreId);
  }
  showGenresList(){
    this.visibleGenresList.next('showed');
  }
  hideGenresList(){
    this.visibleGenresList.next('hidden');
  }
}
