import {Injectable} from "@angular/core";
import {Headers} from '@angular/http';
import config from '../config/env.config'
import {API, Auth} from '../services/index'
import {createURL} from '../lib/Utils'
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class SignUpService {
  constructor (private api: API, private auth: Auth) {}
  public _showModal: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private ROOT_URL = config.API.URL;
  // paths requests
  PATHS = {
    register: this.ROOT_URL+'register'
  };
  /** Requests **/
  register(data, successCallback: any, errorCallback: any) {
    // create URL with params
    let URL = createURL(
      this.PATHS.register,
      {}
    );
    return this.api.post(
      URL,
      data,
      {},
      // reducer callback
      (res: any) => {
        if(!res.success){
          errorCallback(res.errors || null);
        } else {
          successCallback(res.data);
        }
        return { type: null, data: {}}
      },
      (error: any) => (errorCallback(error))
    );
  }
  /** Requests[END] **/
  setShowModal(show = false){
    this._showModal.next(show);
  }
}
