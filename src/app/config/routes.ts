//import {ModalService} from '../services/index';
import {Home, DetailMusic, Profile, TopStations, About, Stations, Page404, Favorites} from '../components/pages/index';

export default [
	{ path: '',
		component: Home,
		pathMatch: 'full'
	},
  {
    path: 'detail-music',
    component: DetailMusic,
    data: { title: 'Detail music' }
  },
  {
    path: 'profile',
    component: Profile,
    data: { title: 'Profile' }
  },
  {
    path: 'top-stations',
    component: TopStations,
    data: { title: 'Top stations List' }
  },
  {
    path: 'about',
    component: About,
    data: { title: 'About us' }
  },
  {
    path: 'stations',
    component: Stations,
    data: { title: 'Genres List' }
  },
	{
		path: 'favorites',
		component: Favorites,
		data: { title: 'Favorites List' }
	},
  { path: '**', component: Page404 }
];
