import {NgModule} from '@angular/core';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {AppComponent}  from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import { ReactiveFormsModule } from '@angular/forms';
import { VirtualScrollModule } from 'angular2-virtual-scroll/dist/virtual-scroll.js';
import { HttpModule } from '@angular/http';
//polyfills
import {exePollyfills} from './lib/Polyfills'
// reducers and store
import {StoreModule} from '@ngrx/store';
import * as Reducers from './state/reducers/index';
// config
import Routes from "./config/routes";
//services
import * as Services from "./services/index";
// pages
import * as pagesComponents from './components/pages/index';
// lists
import * as listComponents from './components/lists/index';
// list items
import * as listItemsComponents from './components/list-items/index';
// main
import * as mainComponents from './components/main/index';

// common
import CommonModule from './components/common/CommonModule'
// modals
import * as ModalsComponents from './components/modals/index'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "i18n/", ".json");
}

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(Routes),
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgbModule.forRoot(),
    StoreModule.provideStore({...Reducers}),
    VirtualScrollModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    // main components
    ...[Object.keys(mainComponents).map(svc => mainComponents[svc])],
    // pages
    ...[Object.keys(pagesComponents).map(svc => pagesComponents[svc])],
    // list
    ...[Object.keys(listComponents).map(svc => listComponents[svc])],
    //list items
    ...[Object.keys(listItemsComponents).map(svc => listItemsComponents[svc])],
    //modals
    ...[Object.keys(ModalsComponents).map(svc => ModalsComponents[svc])]
  ],
  exports: [],
  bootstrap: [AppComponent],
  providers: [Object.keys(Services).map(svc => Services[svc])]
})
export class AppModule {}
