import { Component, ElementRef, ViewChild } from '@angular/core';
import {Auth, SignInService, StationsService, GlobalEventsService} from './services'
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'my-app',
  template: require('./app.template.html'),
	styles: [require('./app.css')]
})
export class AppComponent {
  private authKey: string;
  public userAuthData = null;
	@ViewChild('scrollRef') scrollRef: ElementRef;
	loadingStations = false;
	currentRoute = '';
  subscribes: any = {};
  constructor(
  	private auth: Auth,
	  private signInService:SignInService,
	  private stationsService: StationsService,
	  private globalEventsService: GlobalEventsService,
	  private router: Router){
		this.subscribes.loading = this.stationsService.loading.subscribe((loading) => {
			this.loadingStations = loading;
		});
		this.subscribes.navigations = router.events.subscribe((event) => {
			if(event instanceof NavigationEnd){
				let currentRoute = event.url.split('?')[0];
				if(currentRoute != this.currentRoute){
					this.scrollRef.nativeElement.scrollTo(0,0);
				}
				this.currentRoute = currentRoute;
			}
		});
  };
  ngOnInit() {
// init auth
    this.initAuth();
    this.events();
  }
  ngOnDestroy() {
    this.unsubscribe();
  }
  unsubscribe(){
    for(let key in this.subscribes){
      this.subscribes[key].unsubscribe()
    }
  }
  initAuth() {
    this.auth.getApiKey();
    this.subscribes.auth = this.auth['t'].subscribe(authKey => {
      this.authKey = authKey;
      if(authKey == null){
        this.signInService.logout();
      } else {
        this.signInService.getAuthUserDataRequest();
      }
    });
  }

	onScroll(event){
  	if(this.currentRoute != '/stations'){
  		return;
		}
		let element = event.target;
		if ((element.scrollTop + element.offsetHeight) >= element.scrollHeight) {
			if(!this.loadingStations){
				console.log('stations scroll');
				this.stationsService.getListStations();
			}
		}
	}
	events(){
  	let body = document.getElementsByTagName('body')[0];
		body.addEventListener('mouseup',() => {
			this.globalEventsService.mouseUp.next(true);
		}, false);
		body.addEventListener('mousemove',(event) => {
			this.globalEventsService.setMouseMove(event);
		}, false);
	}
}
